'use client'
// game.tsx the main component for VG2
// TODO:
// - (done) FPS
// - (done) Timer
// - (done-ish) Start/End Game 
// -- Pause?
// - (done) Mouse Controls
// - (done-ish) minions
// - projectiles
// - (done-ish) towers
// - DevTools to spawn minions at different times

// SUNDAY TODO
// Player attacks
// Player sprint (boots)
// Turret
// - targeting minions
// - (done) targeting player
// Minions fighting eachother?
// Floating Damage numbers
// Minions bunching up

// 3/16 TODOs
// (done-ish) Targeting/Focus
// 

// 3/23 TODOs
// Stats, player growth advancement, leveling up
// Shop (rouge lite) - nah
// End goal? defend your crystal/orb
// Single Player - save progress like Townscraper
// 3/24 TODO
// Minimap (look around)
// Player/minion/tower attack warmup/cooldowns
// Your minions and towers
// Abilities!...

// 3/30 TODOs
// fix moving after death
// make respawning take longer each time you die
// add Crystals? at the end of the lane for the enemy to attack, once crystal has 0 health --> Game Over
// 3/31 TODOs (Easter!)
// - different maps/difficulties
// - GameOver screen - match details, time, minions, towers, damage taken, etc...
// 4/6 TODOs
// if you attack a minion who can't see you because of vision/range limitations, then now they can see you?
// 4/13 TODOs
// - Refactor into multiple files? when
// - Abilities
// - Thinking about the end game. do we hard cap at X minutes? Is that adjustable? Infinite mode?
// - Go through old TODOs and pull up the best ones
// --

import React, { useState, useEffect, useRef, Key } from "react";
import styles from "./game.module.css";
import { start } from "repl";
import {useFps} from 'react-fps';
import { relative } from "path";
import next from "next";
import { dir } from "console";
import { request } from "http";
var PF = require('pathfinding'); // require???
const TILE_SIZE = 64;
const PLAYER_SIZE = 32;
const VIEWPORT_PADDING = 100;



const mapData: any = [
  // Map data here, represented as a 2D array of tile types
  // 0 = grass (everyone can walk on these)
  // 1 = wall (nobody can walk through these)
  // 2 = Player1 Health
  // 3 = Player1 Tower
  // 4 = Player2 Health
  // 5 = Player2 Tower
  // 6 = Neutral forest minion?
  
  // TODO: Triangular maps? square maps, hexagons,etc?

  [2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 5, 0, 0, 0, 0, 5, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
  [0, 0, 0, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 6, 1, 0, 0, 0],
  // [0, 0, 0, 3, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 5, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
  // [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  // [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  // [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  // [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],


  // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // 
  // 
  // [0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 1, 0, 0],
  // [1, 0, 1, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 1, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0],
  // [0, 0, 0, 0, 0, 0, 0, 0],
];




const mapWidth = Math.ceil(mapData[0].length);
const mapHeight = mapData.length;
const blankMap = new Array(mapHeight).fill(new Array(mapWidth).fill(0));
// console.log('WIDTH', Math.ceil(blankMap[0].length));

var grid = new PF.Grid(mapData);
grid = grid.clone(); // 
// console.log('grid', grid);
mapData.forEach((row: any, rowIndex: number) => {
  row.forEach((tileType: number, colIndex: number) => {
    if (tileType === 1) {
      grid.nodes[rowIndex][colIndex].Walkable = false;
      mapData[rowIndex][colIndex] = { type: 1, collision: true }; // Wall Tile
    } else if (tileType === 2) {
      grid.nodes[rowIndex][colIndex].Walkable = true;
      mapData[rowIndex][colIndex] = { type: 2, collision: false }; // Health
    } else if (tileType === 3) {
      grid.nodes[rowIndex][colIndex].Walkable = true;
      mapData[rowIndex][colIndex] = { type: 3, collision: true }; // Tower
    } else if (tileType === 4) {
      grid.nodes[rowIndex][colIndex].Walkable = true;
      mapData[rowIndex][colIndex] = { type: 4, collision: false }; // Health (Player2)
    } else if (tileType === 5) {
      grid.nodes[rowIndex][colIndex].Walkable = true;
      mapData[rowIndex][colIndex] = { type: 5, collision: true }; // Tower (Player2)
    } else if (tileType === 6) {
      grid.nodes[rowIndex][colIndex].Walkable = true;
      mapData[rowIndex][colIndex] = { type: 6, collision: false }; // Neutral Minion
    } else {
      grid.nodes[rowIndex][colIndex].Walkable = true;
      mapData[rowIndex][colIndex] = { type: 0, collision: false }; // Walkable (Floor) Tile
    }
  });
});
var gridBackup = grid.clone();

interface KeyPresses {
  [key: string]: boolean;
}
interface KeyPress {
  key: string,
}
interface Coord {
  x: number,
  y: number,
}

function getDistance(a: Coord, b: Coord) {
  // console.log('getDistance', a, b);
  if (a.x == b.x && a.y == b.y) {
    return 0;
  }

  const radx1 = (Math.PI * a.x) / 180;
  const radx2 = (Math.PI * b.x) / 180;

  const theta = a.y - b.y;
  const radtheta = (Math.PI * theta) / 180;

  let dist =
    Math.sin(radx1) * Math.sin(radx2) +
    Math.cos(radx1) * Math.cos(radx2) * Math.cos(radtheta);

  if (dist > 1) {
    dist = 1;
  }

  dist = Math.acos(dist);
  dist = (dist * 180) / Math.PI;
  dist = dist * 60 * 1.1515;
  dist = dist * 1.609344; //convert miles to km
  
  return dist;
}

interface TowerProps {
  // TODO IS a tower just a minion that cant move?
  playerNumber: number, // refers to the numbers up in mapData
  type: string,
  position: {x: number, y: number},
  health: number,
  startingHealth: number,
  strength: number,
  range: number,
  vision: number,
  cooldown: number,
  remainingCooldown: number,
  image: string,
  lastHit: boolean,
}
class Tower implements TowerProps {
  constructor(
    public playerNumber: number,
    public type: string,
    public position: {x: number, y: number},
    public health: number,
    public startingHealth: number,
    public strength: number,
    public range: number,
    public vision: number,
    public cooldown: number,
    public remainingCooldown: number,
    public image: string,
    public lastHit: boolean,
  ) {
    this.playerNumber = playerNumber;
    this.type = type;
    this.position = {x: position.x, y: position.y};
    // console.log('POSITION:', this.position);
    this.health = health;
    this.startingHealth = startingHealth;
    this.strength = strength;
    this.range = range;
    this.vision = vision;
    this.cooldown = cooldown;
    this.remainingCooldown = 0;
    this.image = image;
    this.lastHit = playerStrength >= startingHealth; // only true when the player could K.O. on next attack
  }
  move(delta: number, grid: any, playerPosition: {x: number, y: number}, doAttackPlayer: any, isRespawning: boolean, am: any): void {
    if (this.health <= 0) return;
    if (this.remainingCooldown > 0) {
      this.remainingCooldown = this.remainingCooldown - delta/1000
    } else if (this.remainingCooldown < 0) {
      this.remainingCooldown = 0;
    }
    let target = false; // chill towers
    let hasAttacked = false;
    if (this.playerNumber === 3) {
      // console.log('Player Tower Move', this.health, this.remainingCooldown, this.playerNumber);

    } else {
      // console.log('Enemy Tower Move', this.health, this.remainingCooldown, this.playerNumber);
      const distanceToPlayer = getDistance(this.position, playerPosition);
      if (distanceToPlayer < this.range && this.remainingCooldown <= 0) {
        console.log('Gonna attack the player')
        this.attack(doAttackPlayer, 'player');
        this.remainingCooldown = this.cooldown;
        hasAttacked = true;
      }
    }
    if (!hasAttacked) {
      // Default target is the middle row on the second to last column
      const currentPosition = this.position;
      let closestMinionCount = 0;
      let closestMinionDistance = Infinity;
      am.map((m: Minion, count: number) => {
        const d = getDistance(currentPosition, m.position)
        if (d < closestMinionDistance && m.health > 0) {
          closestMinionCount = count;
          closestMinionDistance = d;
        }
      });
      // const distanceToPlayer = getDistance(this.position, playerPosition);

      if (closestMinionDistance > this.range) { // Can the minion attack the player?
        // console.log('_out of range_')
      } else {
        if (!hasAttacked && !isRespawning && this.remainingCooldown <= 0) { // and didn't just attack
          // this.attack(doAttackPlayer, 'player')
          am[closestMinionCount].getAttacked(this.strength);
          // console.log('attacking...', );
          if (am[closestMinionCount].health < playerStrength) {
            am[closestMinionCount].primeLastHit()
          }
          this.remainingCooldown = this.cooldown;
        }
      }
    }
  }
  attack(doAttackPlayer: any, target: any): void {
    // console.log('ATTACK', doAttackPlayer, target);
    this.remainingCooldown = this.cooldown;
    doAttackPlayer(this.strength)();
    // if (this.health - 1 < 0) {
    //   this.health = 0;
    // } else {
    //   this.health = this.health - 1;
    // }
  }
  getAttacked(strength: number): void {
    this.health = this.health - strength;
    // console.log('Tower - getAttacked', strength, this.health);
  }
  primeLastHit(): void {
    this.lastHit = true;
  }
}


const startA = {x: mapWidth - 1, y: Math.floor(mapHeight / 2)};
const startB = {x: 1, y: Math.floor(mapHeight / 2)};

interface MinionProps {
  playerNumber: number | string,
  type: string,
  position: {x: number, y: number},
  startingPosition: {x: number, y: number},
  health: number,
  startingHealth: number,
  strength: number,
  cooldown: number,
  remainingCooldown: number,
  speed: number,
  image: string,
  path: {x: number, y: number}[],
  lastHit: boolean,
}
class Minion implements MinionProps {
  constructor(
    public playerNumber: number | string,
    public type: string,
    public position: {x: number, y: number},
    public startingPosition: {x: number, y: number},
    public health: number,
    public startingHealth: number,
    public strength: number,
    public range: number,
    public vision: number,
    public speed: number,
    public cooldown: number,
    public remainingCooldown: number,
    // private lastAttackAt: any,
    public image: string,
    // public finder: any,
    public path: any,
    public direction: number, // radians? degrees?
    public lastHit: boolean,
    public forestMinion: boolean,
  ) {
    this.playerNumber = playerNumber;
    this.type = type;
    this.position = {x: position.x, y: position.y};
    this.startingPosition = {x: position.x, y: position.y};
    // console.log('POSITION:', this.position);
    this.health = health;
    this.startingHealth = startingHealth;
    this.strength = strength;
    this.range = range;
    this.vision = vision;
    this.speed = speed;
    this.cooldown = cooldown;
    this.remainingCooldown = 0;
    // this.lastAttackAt = false;
    this.image = image;
    // this.finder = finder;
    // console.log('constructor Path')
    // const newPath = finder.findPath(Math.floor(this.position.x), Math.floor(this.position.y), 0, 0, grid);
    // console.log('newPath', newPath);
    this.path = [];
    this.direction = 0;
    this.lastHit = playerStrength >= startingHealth;
    this.forestMinion = forestMinion;
  }
  move(delta: number, grid: any, playerPosition: {x: number, y: number}, doAttackPlayer: any, isRespawning: boolean, mt: any, mm: any): void {
    
    if (this.health <= 0) {
      this.path = [];
      return
    }
    if (this.remainingCooldown > 0) {
      this.remainingCooldown = this.remainingCooldown - delta/1000
    }
    if (this.remainingCooldown < 0) {
      this.remainingCooldown = 0;
    }
    grid = gridBackup.clone();
    // Default target is the middle row on the second to last column
    let target = this.playerNumber.toString() !== '2' ? startB : startA;
    const currentPosition = this.position;
    const distanceToPlayer = getDistance(this.position, playerPosition);

    let closestMinionCount = 0;
    let closestMinionDistance = Infinity;
    mm.map((m: Minion, count: number) => {
      const d = getDistance(currentPosition, m.position)
      if (d < closestMinionDistance && m.health > 0) {
        closestMinionCount = count;
        closestMinionDistance = d;
      }
    });
    const realClosestMinionDistance = closestMinionDistance;
    let closestTowerCount = 0;
    let closestTowerDistance = Infinity;
    mt.map((t: Tower, count: number) => {
      const d = getDistance(currentPosition, t.position)
      if (d < closestTowerDistance && t.health > 0) {
        closestTowerCount = count;
        closestTowerDistance = d;
      }
    });
    
    // Can the minion see the player?
    let targetingATower = false;
    let targetingAMinion = false;
    if (realClosestMinionDistance < this.vision) {
      // console.log('Target Minion', this.playerNumber, this.remainingCooldown)
      target = mm[closestMinionCount].position;
      targetingAMinion = true;
    } else if (closestTowerDistance < this.vision) {
      // console.log('Target Tower', this.playerNumber, this.remainingCooldown)
      target = mt[closestTowerCount].position;
      // console.log('Target Tower', target)
      targetingATower = true;
    } else if (this.playerNumber !== 2 && distanceToPlayer < this.vision && !isRespawning) {
      // console.log('Target Player', this.playerNumber, this.remainingCooldown)
      target = playerPosition;
    } else {
      if (this.forestMinion) {
        target = this.startingPosition;
      } else {
        target = this.playerNumber !== 2 ? startB : startA;
      }
    }
    if (((distanceToPlayer > this.range || this.playerNumber === 2) && closestTowerDistance > this.range && realClosestMinionDistance > this.range)) { // Can the minion attack the player?
      // console.log('everyone is out of range, move forward');
      if (currentPosition) {
        if (currentPosition.x < mapWidth && currentPosition.x > 0) {
          if (!grid) {
            grid = gridBackup.clone()
          }
          let path = aStarFinder.findPath(Math.floor(this.position.x), Math.floor(this.position.y), Math.floor(target.x), Math.floor(target.y), grid);
          if (targetingATower) {
            // console.log('whooowowo', target, grid);
            grid = gridBackup.clone()
            path = aStarFinder.findPath(Math.floor(this.position.x), Math.floor(this.position.y), Math.floor(target.x-0.5), Math.floor(target.y), grid);
            // if (this.playerNumber === 2) {
            // } else {
            //   const wtf = Math.floor(Math.random()*4);
            //   console.log('THIs position', this.position, target);
            //   path = aStarFinder.findPath(Math.floor(this.position.x), Math.floor(this.position.y), Math.floor(target.x+2), Math.floor(target.y-1), grid);
            // }
          }
          // console.log('123', path);
          
          const movementSpeed = this.speed * delta / 1000; // Adjust speed as needed       
          if (path && path.length > 1) {
            // console.log('short path');
            let currentDistanceToTarget = Math.abs(currentPosition.x - target.x) + Math.abs(currentPosition.y - target.y)
            // if (this.forestMinion) {
            //   console.log('currentDistanceToTarget', currentDistanceToTarget);
            // }
            const nextIndex = Math.min(
              path.findIndex((point: any) => {
                const distanceX = point[0] - currentPosition.x;
                const distanceY = point[1] - currentPosition.y;
                // Prioritize points closer to the target in case of overlap
                const distanceToTarget = Math.abs(point[0] - target.x) + Math.abs(point[1] - target.y);
                // console.log('x', point, distanceX, distanceX, distanceToTarget);
                return distanceX < 0 && distanceY < 0 || distanceToTarget < currentDistanceToTarget;
              }),
              path.length - 1 // Ensure index stays within bounds
            );
            currentDistanceToTarget = Math.abs(path[nextIndex][0] - target.x) + Math.abs(path[nextIndex][1] - target.y)
            let nextPoint = path[nextIndex];
            if (path.length > 1) {
              // console.log('very hacky - long path');
              nextPoint = path[nextIndex + 1] // kinda hacky
            }
            // let minX = this.position.x;
            // let maxX = this.position.x;
            // let minY = this.position.y;
            // let maxY = this.position.y;
            // for (const p of path) {
            //   minX = Math.min(minX, p[0]);
            //   maxX = Math.max(maxX, p[0]);
            //   minY = Math.min(minY, p[1]);
            //   maxY = Math.max(maxY, p[1]);
            // }
            // let direction = {
            //   x: (Math.floor(nextPoint[0]) - Math.floor(currentPosition.x)) / (maxX - minX),
            //   y: (Math.floor(nextPoint[1]) - Math.floor(currentPosition.y)) / (maxY - minY),
            // }
            let direction = {
              x: 0,
              y: 0,
            }
            if (nextPoint !== undefined) {
              direction = {
                x: ((nextPoint[0] + 0.25) - currentPosition.x),
                y: ((nextPoint[1] + 0.25) - currentPosition.y),
              }
            }
            
            this.direction = (360 + Math.round(180 * Math.atan2(direction.x, direction.y) / Math.PI))%360; // x and y are flipped here! on purpos
            const magnitude = Math.sqrt(direction.x * direction.x + direction.y * direction.y)
            const mvmt = {
              x: (direction.x / magnitude) * movementSpeed,
              y: (direction.y / magnitude) * movementSpeed,
            }
            // if (this.forestMinion) {
            //   console.log('Path', mvmt)
            // };
            const randAmt = 0; (1-(2*Math.random()))/50;
            const nextX = currentPosition.x + mvmt.x + randAmt
            const nextY = currentPosition.y + mvmt.y + randAmt;
            if (nextX < mapWidth && nextX > 0) {
              this.position.x = nextX;
            }
            if (nextY < mapHeight && nextY > 0) {
              this.position.y = nextY;
            }
            this.path = path;
          } else {
            
            // console.log('No Path!', this.position, target);
            let direction = {
              x: ((target.x + 0.25) - currentPosition.x),
              y: ((target.y + 0.25) - currentPosition.y),
            }
            
            this.direction = (360 + Math.round(180 * Math.atan2(direction.x, direction.y) / Math.PI))%360; // x and y are flipped here! on purpos
            const magnitude = Math.sqrt(direction.x * direction.x + direction.y * direction.y)
            const mvmt = {
              x: (direction.x / magnitude) * movementSpeed,
              y: (direction.y / magnitude) * movementSpeed,
            }
            if (this.forestMinion) {
              if (Math.abs(currentPosition.x - target.x) < 0.5) {
                mvmt.x = 0;
              }
              if (Math.abs(currentPosition.y - target.y) < 0.5) {
                mvmt.y = 0;
              }
              // let currentDistanceToTarget = Math.abs(currentPosition.x - target.x) + Math.abs(currentPosition.y - target.y)
              // console.log('No path', currentDistanceToTarget)
            };
            const randAmt = 0; //(1-(2*Math.random()))/50;
            const nextX = currentPosition.x + mvmt.x + randAmt
            const nextY = currentPosition.y + mvmt.y + randAmt;
            // console.log('mvmt', mvmt);
            if (nextX < mapWidth && nextX > 0) {
              this.position.x = nextX;
            }
            if (nextY < mapHeight && nextY > 0) {
              this.position.y = nextY;
            }

            // console.log(playerPosition)
            this.path = [];
          }
          
        }
      }
    } else {
      // console.log()
      if (this.remainingCooldown <= 0) { // and didn't just attack
        // this.lastAttackAt = delta;
        if (realClosestMinionDistance < this.range) {
          // console.log('ATTACK a Minion')
          mm[closestMinionCount].getAttacked(this.strength);
          if (mm[closestMinionCount].health < playerStrength) {
            mm[closestMinionCount].primeLastHit()
          }
          this.remainingCooldown = this.cooldown;
        } else if (!isRespawning && this.playerNumber !== 2 && distanceToPlayer < this.range) {
            // console.log('ATTACK THE player')
            this.attack(doAttackPlayer, 'player');
        } else if (closestTowerDistance < this.range) {
          // console.log('ATTACK THE TOWER')
          mt[closestTowerCount].getAttacked(this.strength);
          this.remainingCooldown = this.cooldown;
        }
      }
    }
  }
  attack(doAttackPlayer: any, target: any): void {
    // console.log('ATTACK', doAttackPlayer, target);
    this.remainingCooldown = this.cooldown;
    doAttackPlayer(this.strength)();
    // if (this.health - 1 < 0) {
    //   this.health = 0;
    // } else {
    //   this.health = this.health - 1;
    // }
  }
  getAttacked(strength: number): void {
    this.health = this.health - strength;
    // console.log('getAttacked', strength, this.health);
  }
  primeLastHit(): void {
    this.lastHit = true;
  }
}

interface MinionType {
  playerNumber: number | string;
  type: string;
  health: number;
  // startingHealth: number;
  strength: number;
  range: number;
  vision: number;
  speed: number;
  cooldown: number;
  // remainingCooldown: number,
  image: string;
  // path: Array<[number, number]> | [];
  // direction: number,
}
const aStarFinder = new PF.AStarFinder({
  allowDiagonal: true,
  dontCrossCorners: true,
});
class TowerFactory {
  private listeners: ((tower: Tower) => void)[] = [];
  onNewTower(listener: (tower: Tower) => void): void {
    this.listeners.push(listener);
    // console.log('onNewTower', this.towerType, this.listeners); 
  }
  offNewTower(listener: (tower: Tower) => void): void {
    // console.log('offNewTower !!!');
    this.listeners = this.listeners.filter((l) => l !== listener);
  }
  constructor(public towerType: number) {
    this.towerType = towerType;
    // console.log('Hey now');
  }
  spawnTowers(): void {
    // console.log('Spawning Towers...', mapData);
    mapData.map((row: any, y: number) => {
      row.map((row: any, x: number) => {
        const {type: tileType, collision} = row;

        if (tileType === this.towerType) {
          const newTower = new Tower(
            this.towerType,
            'base',
            {x: x, y: y},
            500, // health
            500, // health
            100, // strength
            350, // range
            500, // vision ???
            4, // cooldown
            0, // remaining cooldown
            'tower.png', // image
            false, // lastHit
          )
          this.listeners.forEach((listener) => listener(newTower));
        }
      })
    })
  }
  clearTowers(): void {
    console.log('Clear Towers');
  }
}
class MinionFactory {
  private listeners: ((minion: Minion) => void)[] = [];

  onNewMinion(listener: (minion: Minion) => void): void {
    this.listeners.push(listener);
    // console.log('onNewMinion', this.listeners);
    
  }
  offNewMinion(listener: (minion: Minion) => void): void {
    console.log('offNewMinion !!!');
    this.listeners = this.listeners.filter((l) => l !== listener);
  }
  constructor(
    private playerNumber: number,
    private spawnPoint: {x: number, y: number},
    private waveInterval: number,
    private minionTypes: MinionType[],
  ) {
    this.playerNumber = playerNumber;
    this.spawnPoint = spawnPoint;
    this.waveInterval = waveInterval;
    this.minionTypes = minionTypes;
  }
  spawnOne(): void {
    // console.log('SPAWN ONE')
    const chosenType = this.chooseMinionType();
    const newMinion = new Minion(
      this.playerNumber,
      chosenType.type,
      this.spawnPoint,
      this.spawnPoint,
      chosenType.health,
      chosenType.health, // starting Health
      chosenType.strength,
      chosenType.range,
      chosenType.vision,
      chosenType.speed,
      chosenType.cooldown,
      0,
      chosenType.image,
      [], 0, false, false,
    );
    this.listeners.forEach((listener) => listener(newMinion))
  }
  startSpawning(): void {
    console.log('STARTED_SPAWNING', this.playerNumber);
    let count = 0;
    setInterval(() => {
      count = count + 1
      // console.log('SPAWN - ', count);
      if (count > 25) return;
      this.spawnOne();
      // const chosenType = this.chooseMinionType();
      // const newMinion = new Minion(
      //   this.playerNumber,
      //   chosenType.type,
      //   this.spawnPoint,
      //   chosenType.health,
      //   chosenType.health, // starting Health
      //   chosenType.strength,
      //   chosenType.range,
      //   chosenType.vision,
      //   chosenType.speed,
      //   chosenType.cooldown,
      //   0,
      //   chosenType.image,
      //   [], 0, false
      // );
      // this.listeners.forEach((listener) => listener(newMinion))
    }, this.waveInterval)
  }
  spawnForestMinions(): void {
    mapData.map((row: any, y: number) => {
      row.map((row: any, x: number) => {
        const {type: tileType, collision} = row;

        if (tileType === 6) {
          const forestSpawn = {x: x, y: y};
          const chosenType = this.chooseMinionType();
          const newMinion = new Minion(
            this.playerNumber,
            chosenType.type,
            forestSpawn,
            forestSpawn,
            chosenType.health,
            chosenType.health, // starting Health
            chosenType.strength,
            chosenType.range,
            chosenType.vision, //*2, // double vision for forest misions?
            chosenType.speed,
            chosenType.cooldown,
            0,
            chosenType.image,
            [], 0, false, true,
          );
          this.listeners.forEach((listener) => listener(newMinion))
        }
      })
    })
    
  }
  chooseMinionType(): MinionType {
    const r = Math.floor(Math.random() * this.minionTypes.length);
    return this.minionTypes[r];
  }
  clearMinions(): void {
    console.log('Clear Minions');
  }
}

const terribleMinionTypes: any[] = [
  {
    type: 'basic',
    health: 105, // starting health is always 100%
    strength: 1,
    range: TILE_SIZE*1.5,
    vision: 500,
    speed: 2,
    cooldown: 1,
    image: 'uhhh.png',
   }, 
]
const basicMinionTypes: any[] = [
 {
  type: 'basic',
  health: 120, // starting health is always 100%
  strength: 7,
  range: TILE_SIZE*1.5,
  vision: TILE_SIZE*2.5*2,
  speed: 0.75,
  cooldown: 0.75,
  image: 'uhhh.png',
 },
//  {
//   type: 'expert',
//   health: 105, // starting health is always 100%
//   strength: 15,
//   range: TILE_SIZE*1.5,
//   vision: 500,
//   speed: 0.82,
//   cooldown: 0.95,
//   image: 'uhhh.png',
//  },
//  {
//   type: 'lvl2',
//   health: 110,
//   strength: 5,
//   range: TILE_SIZE*1.75,
//   vision: 550,
//   cooldown: 0.76,
//   speed: 1.1,
//   image: 'uhhh2.png',
//  },
//  {
//   type: 'lvl2',//2a
//   health: 90,
//   strength: 7,
//   range: 100,
//   vision: 600,
//   cooldown: 1,
//   speed: 1.3,
//   image: 'uhhh2.png',
//  },
//  {
//   type: 'lvl3',
//   health: 200,
//   strength: 6,
//   range: 120,
//   vision: 700,
//   cooldown: 3,
//   speed: 1.2,
//   image: 'uhhh2.png',
//  },
//  {
//   type: 'lvl4',
//   health: 50,
//   strength: 8,
//   range: 140,
//   vision: 500,
//   cooldown: 5,
//   speed: 0.9,
//   image: 'uhhh2.png',
//  },
];


const playerStartingHealth = 1000;
const playerStrength = 23;
const playerStartingCooldown = 0.76;
const playerRange = 275;
const playerTapRange = PLAYER_SIZE*4;

type ClickCoords = {x: number, y: number} | false;

export function Game() {
  const [startTime, setStartTime] = useState<number>(0);
  const [gameOverTime, setGameOverTime] = useState<number>(0);
  const [gameOver, setGameOver] = useState<boolean>(true);
  let go: boolean = false;
  const [gameState, setGameState] = useState<string>('play');
  const [lastHitCount, setLastHitCount] = useState<number>(0);
  let lhc: number = 0;
  const [curTarget, setCurTarget] = useState<any>(false);
  let ct: any = false;
  const [curTargetType, setCurTargetType] = useState<any>(false);
  let ctt: any = false;
  const [clickCoords, setClickCoords] = useState<ClickCoords>(false)
  let cc: any = false;
  const [curPath, setCurPath] = useState([]);
  const [isRespawning, setIsRespawning] = useState(false);
  let ir = false;
  const [playerHealth, setPlayerHealth] = useState(playerStartingHealth);
  let ph = playerStartingHealth;
  const [pCooldown, setPCooldown] = useState(0);
  let playerCooldown = 0;
  const [playerX, setPlayerX] = useState(0.5);
  const [playerY, setPlayerY] = useState(0.5);
  let px = 0.5;
  let py = 0.5;

  const [minionX, setMinionX] = useState(20);
  const [minionY, setMinionY] = useState(0.5);
  let mx = 33;
  let my = 3;
  

  // Enemy TOWERS AND MINIONS
  const [activeTowers, setActiveTowers] = useState<Tower[]>([]);
  let at: any = [];
  const [activeMinions, setActiveMinions] = useState<Minion[]>([]);
  let am: any = [];
  // Player Towers AND Minions
  const [myTowers, setMyTowers] = useState<Tower[]>([]);
  let mt: any = [];
  const [myMinions, setMyMinions] = useState<Minion[]>([]);
  let mm: any = [];

  const mapRef = useRef(null);
  const miniMapRef = useRef(null);
  const playerRef = useRef(null);
  // const minionRef = useRef(null);
  let requestId = 0; // Stores the animation frame ID
  let lastTimestamp: any = null;
  const [gameTime, setGameTime] = useState(0);
  const keysPressed: KeyPresses = {};
  

  const startGameLoop = () => {
    console.log('Starting the GameLoop');
    // Schedule the next animation frame
    requestId = requestAnimationFrame(updateGame);
    // console.log('reqId', requestId);
  };

  const stopGameLoop = () => {
    console.log('STOP THE GAME LOOP')
    // Cancel the scheduled animation frame
    if (requestId) {
      cancelAnimationFrame(requestId);
      requestId = 0;
    }
  };

  // Function to handle keyboard input
  function handleKeyDown(event: KeyPress) {
    keysPressed[event.key] = true;
    setClickCoords(false);
    setCurTarget(false);
    ct = false;
    setCurTargetType(false);
    ctt = false;
    cc = false;
    // TODO: Would be nice to start the game with spacebar or something?
    // console.log('keydown', event.key, gameOver);
    // if (event.key === ' ' && gameOver) {
    //   console.log('Restarting the game')
    //   restartTheGame();
    // }
    
  }
  function handleKeyUp(event: KeyPress) {
    delete keysPressed[event.key];
  }
  function handleTouchStart(event: any) {
    // console.log('Touch start', event);
    // TODO:
  }
  function handleMouseDown(event: any) {
  
    let newX = (((0.5 + event.clientX) / TILE_SIZE) - (window.innerWidth/TILE_SIZE/2) + (px)); // Used to have Math.floor
    if (newX < 0) newX = 0;
    if (newX >= mapWidth) newX = mapWidth;
    
    let newY = (((0.5 + event.clientY) / TILE_SIZE) - (window.innerHeight/TILE_SIZE/2) + (py)); // Used to have Math.floor
    if (newY < 0) newY = 0;
    if (newY >= mapHeight) newY = mapHeight;
    const newCoords = {x: newX, y: newY};
    // console.log('newCoords', newCoords);
    // console.log('Mouse down', newCoords, px);
    setClickCoords(newCoords);

    
    let closestMinionCount = 0;
    let closestMinionDistance = Infinity;
    am.map((m: Minion, count: number) => {
      const d = getDistance(newCoords, m.position)
      if (d < closestMinionDistance && m.health > 0) {
        closestMinionCount = count;
        closestMinionDistance = d;
      }
    });

    let closestTowerCount = 0;
    let closestTowerDistance = Infinity;
    at.map((t: Tower, count: number) => {
      const d = getDistance(newCoords, t.position)
      if (d < closestTowerDistance && t.health > 0) {
        closestTowerCount = count;
        closestTowerDistance = d;
      }
    });
    // console.log('closest tower', closestTowerDistance);
    if (closestTowerDistance < playerTapRange) {
      setCurTarget(closestTowerCount);
      ct = closestTowerCount
      setCurTargetType('tower');
      ctt = 'tower';
      
    } else {
      // console.log('no towers near click');
      // console.log('closest minion', closestMinionDistance);
      if (closestMinionDistance < playerTapRange) {
        // TODO: Player gets better range
        setCurTarget(closestMinionCount)
        ct = closestMinionCount;
        setCurTargetType('minion');
        ctt = 'minion';
      } else {
        setCurTarget(false);
        ct = false;
        setCurTargetType(false);
        ctt = false;
      }
    }

    // TODO: Player pathing
    // const newerGrid = grid.clone()
    // const path = aStarFinder.findPath(
    //   Math.floor(playerX), Math.floor(playerY),
    //   Math.floor(newCoords.x), Math.floor(newCoords.y),
    //   newerGrid
    // );
    // console.log('path', path);
    // if (path) setCurPath(path);
    
    cc = newCoords;
  }
  function handleInput() {
    
    window.addEventListener('keydown', handleKeyDown);
    window.addEventListener('keyup', handleKeyUp);
    // document.addEventListener('touchstart', handleTouchStart);
    document.addEventListener('mousedown', handleMouseDown);
  }
  function unhandleInput() {
    window.removeEventListener('keydown', handleKeyDown);
    window.removeEventListener('keyup', handleKeyUp);
    // document.removeEventListener('touchstart', handleTouchStart);
    document.removeEventListener('mousedown', handleMouseDown);
    console.log('....UnhandleInputs........')
  }

  function getAnyFolksNearby(x: number, y: number) {
    // console.log('getAnyFolksNearby', am, at);
    const newCoords = {x: x, y: y};
    let res: any = {};
    let closestMinionCount = 0;
    let closestMinionDistance = Infinity;
    am.map((m: Minion, count: number) => {
      const d = getDistance(newCoords, m.position)
      if (d < closestMinionDistance && m.health > 0) {
        closestMinionCount = count;
        closestMinionDistance = d;
      }
    });

    let closestTowerCount = 0;
    let closestTowerDistance = Infinity;
    at.map((t: Tower, count: number) => {
      const d = getDistance(newCoords, t.position)
      if (d < closestTowerDistance && t.health > 0) {
        closestTowerCount = count;
        closestTowerDistance = d;
      }
    });
    // console.log('Next Closest Minion', closestMinionDistance);
    // console.log('Next Closest Tower', closestTowerDistance);
    if (closestMinionDistance > playerRange && closestTowerDistance > playerRange) {
      return false;
    }
    if (closestMinionDistance <= playerRange) {
      res.targetType = 'minion';
      res.target = closestMinionCount;
    }
    if (closestTowerDistance <= playerRange) {
      res.targetType = 'tower';
      res.target = closestTowerCount;
    }
    return res;
  }

  const updateGame = async(timestamp: number) => {
    // await new Promise(r => setTimeout(r, 10));
    // Calculate time delta since last frame
    const delta = timestamp - (lastTimestamp || timestamp);
    lastTimestamp = timestamp;
    // console.log('lastTimestamp', lastTimestamp);
    setGameTime(lastTimestamp);
    requestId = 0;

    // Update game state based on delta
    updateGameState(delta, lastTimestamp, keysPressed);

    // Render the game based on the updated state
    // renderGame();

    // Schedule the next animation frame
    requestId = requestAnimationFrame(updateGame);
  };

  // ... other game logic ...
  function checkType(x: number, y: number) {
    // Get the tile coordinates based on the player's intended position
    const tileX = Math.floor(x);
    const tileY = Math.floor(y);
    // console.log('tileXY', x, y, tileX, tileY);
    // Check if the tile coordinates are within the map bounds
    // if (tileX < 0 || tileX >= mapData[0].length || tileY < 0 || tileY >= mapData.length) {
    //   return true; // Consider treating out-of-bounds as collisions
    // }
    // Access the tile data and check its collision property
    return mapData[tileY][tileX].type;
  }
  function checkCollision(x: number, y: number) {
    // Get the tile coordinates based on the player's intended position
    const tileX = Math.floor(x);
    const tileY = Math.floor(y);
    // console.log('tileXY', x, y, tileX, tileY);
    // Check if the tile coordinates are within the map bounds
    // if (tileX < 0 || tileX >= mapData[0].length || tileY < 0 || tileY >= mapData.length) {
    //   return true; // Consider treating out-of-bounds as collisions
    // }
    // Access the tile data and check its collision property
    return mapData[tileY][tileX].collision;
  }

  // TODO
  function updateMinionState(delta: number, lastTimestamp: number, newGrid: object[], playerPosition: Coord) {
    const movementSpeed = 0.05 * delta / 100; // Adjust speed as needed

    // if (mx > 0) {
    //   mx = mx - movementSpeed;
    // } else {
    //   mx = 33;
    // }
    // setMinionX(mx);
    const doAttackPlayer = (strength: number) => () => {
      // console.log('Do attack', ph, playerHealth, ' - ', strength);
      const newHealth = ph - strength;
      if (newHealth <= 0) {
        // console.log('DEAD');
        setPlayerHealth(0);
        ir = true;
        if (!isRespawning || !ir) {
          setTimeout(() => {
            console.log('respawn')
            setPlayerX(0.5);
            setPlayerY(0.5);
            px = 0.5;
            py = 0.5;
            setPlayerHealth(playerStartingHealth);

            setClickCoords(false);
            cc = false;
            ph = playerStartingHealth;
            ir = false;
          }, 5000) // respawn takes 5s
        } else {
          console.log('already dead')
        }
      } else {
        setPlayerHealth(ph - strength);
      }
      
      ph = ph - strength;

    }
    let remainingEnemyTowers = 0;
    let remainingPlayerTowers = 0;
    at.map((t: Tower) => t.move(delta, newGrid, playerPosition, doAttackPlayer, ir, mm));
    am.map((m: Minion) => m.move(delta, newGrid, playerPosition, doAttackPlayer, ir, mt, mm));
    mt.map((t: Tower) => t.move(delta, newGrid, playerPosition, doAttackPlayer, ir, am))
    mm.map((m: Minion) => m.move(delta, newGrid, playerPosition, doAttackPlayer, ir, at, am));
    at.map((x: Tower) => {
      if (x.health > 0) remainingEnemyTowers += 1;
    });
    mt.map((x: Tower) => {
      if (x.health > 0) remainingPlayerTowers += 1;
    });
    
    if (remainingEnemyTowers < 1 && !go) { // Player Wins!
      console.log('Player Wins');
      setGameOver(true);
      setGameOverTime(lastTimestamp);
      go = true;
      setGameState('Victory');
    }
    if (remainingPlayerTowers < 1 && !go) { // Player loses
      console.log('Player Loses');
      setGameOver(true)
      go = true;
      setGameState('Defeat');
    }
    // if (.length > 0) {
    //   console.log('no more active towers')
    // }
    // if (mt.filter((x: Tower) => (x.health > 0)).length > 0) {
    //   console.log('no more My towers')
    // }
    // console.log(Math.abs(Math.floor(px - mx)), Math.abs(Math.floor(py - my)))
    // if (Math.abs(Math.floor(px - mx)) < 1 && Math.abs(Math.floor(py - my)) < 1) {
    //   console.log('wow shit collisions')
    // }
  }
  // Function to update game state based on delta and pressed keys
  function updateGameState(delta: number, lastTimestamp: number, keysPressed: KeyPresses) {
    // Handle player movement based on pressed keys (WASD)
    const movementSpeed = 0.15 * delta / 100; // Adjust speed as needed
    if (playerCooldown > 0 ) {
      playerCooldown = playerCooldown - delta/1000;
      // setPCooldown(pCooldown - delta/1000)
    }
    if (!isRespawning) {
      if (keysPressed['w'] || keysPressed['ArrowUp']) {
        // console.log('W', py, playerY, movementSpeed)
        // Move player up
        if (py - movementSpeed > 0 && !checkCollision(px, py - movementSpeed)) {
          py = py - movementSpeed;
        }
      }
      if (keysPressed['a'] || keysPressed['ArrowLeft']) {
        // console.log('A', px, movementSpeed)
        // Move player left
        if (px - movementSpeed > 0 && !checkCollision(px - movementSpeed, py)) {
          px = px - movementSpeed;
        }
      }
      if (keysPressed['s'] || keysPressed['ArrowDown']) {
        // console.log('S', py, playerY, movementSpeed, mapHeight)
        // Move player down
        if (py + movementSpeed < mapHeight && !checkCollision(px, py + movementSpeed)) {
          py = py + movementSpeed;
        }
      }
      if (keysPressed['d'] || keysPressed['ArrowRight']) {
        // console.log('D', px, movementSpeed, mapWidth)
        // Move player right
        if (px + movementSpeed < mapWidth && !checkCollision(px + movementSpeed, py)) {
          px = px + movementSpeed;
        }
      }
      // console.log('ClickCoords', cc, clickCoords);
      if (cc !== false) {
        // console.log(curPath); // TODO: Path finding around obstacles
        let inRangeToAttack = false;
        if (ct !== false && ctt !== false) {
          if (ctt === 'tower') {
            const d = getDistance({x: px, y: py}, at[ct].position);
            if (d < playerRange) {
              inRangeToAttack = true;
              if (playerCooldown <= 0) {
                at[ct].getAttacked(playerStrength);
                playerCooldown = playerStartingCooldown;
                // TODO: Double-check logic for last hits.
                if (at[ct].health < 0) {
                  let anyFolksNearby: any = getAnyFolksNearby(px, py);
                  if (anyFolksNearby !== false) {
                    setCurTarget(anyFolksNearby.target);
                    ct = anyFolksNearby.target;
                    setCurTargetType(anyFolksNearby.targetType);
                    ctt = anyFolksNearby.targetType
                    const newClickEms = anyFolksNearby.targetType === 'tower' ? at[anyFolksNearby.target].position : am[anyFolksNearby.target].position;
                    setClickCoords(newClickEms);
                    cc = newClickEms;
                    // console.log('_____NEW TARGET_____')
                  } else {
                    setCurTarget(false);
                    ct = false;
                    setCurTargetType(false);
                    ctt = false;
                    setClickCoords(false);
                    cc = false;
                    // console.log('Nobody nearby')
                  }
                  // console.log('Dead', lhc, lastHitCount);
                  lhc = lhc + 1;
                  setLastHitCount(lhc);
                  // TODO: if k.o. target next closest minion/tower in range.
                } else if (at[ct].health < playerStrength) {
                  // console.log('LAST HIT');
                  at[ct].primeLastHit()
                }
              }
            }
          } else if (ctt === 'minion') {
            const d = getDistance({x: px, y: py}, am[ct].position);
            if (d < playerRange) {
              inRangeToAttack = true;
              if (playerCooldown <= 0) {
                am[ct].getAttacked(playerStrength);
                playerCooldown = playerStartingCooldown;
                if (am[ct].health < 0) {
                  let anyFolksNearby: any = getAnyFolksNearby(px, py);
                  if (anyFolksNearby !== false) {
                    setCurTarget(anyFolksNearby.target);
                    ct = anyFolksNearby.target;
                    setCurTargetType(anyFolksNearby.targetType);
                    ctt = anyFolksNearby.targetType
                    const newClickEms = anyFolksNearby.targetType === 'tower' ? at[anyFolksNearby.target].position : am[anyFolksNearby.target].position;
                    setClickCoords(newClickEms);
                    cc = newClickEms;
                    // console.log('_____NEW TARGET_____')
                  } else {
                    setCurTarget(false);
                    ct = false;
                    setCurTargetType(false);
                    ctt = false;
                    setClickCoords(false);
                    cc = false;
                    // console.log('Nobody nearby')
                  }
                  // console.log('Dead', lhc, lastHitCount);
                  lhc = lhc + 1;
                  setLastHitCount(lhc);
                  // TODO: if k.o. target next closest minion/tower in range.
                } else if (am[ct].health < playerStrength) {
                  // console.log('LAST HIT');
                  am[ct].primeLastHit();
                }
              }
            }
          }
        }
        
        if (!inRangeToAttack) {
          const midX = mapWidth / 2;
          const midY = mapHeight / 2;
          const dx = cc.x - px + 0.5;
          const dy = cc.y - py + 0.5;
          const normal = Math.sqrt(dx * dx + dy * dy);
          const uDx = dx / normal;
          const uDy = dy / normal;
          const sDx = uDx * movementSpeed;
          const sDy = uDy * movementSpeed;

          // console.log('P', dx, dy)

          if (Math.abs(dx) > 0.025 && px + sDx > 0 && px + sDx < mapWidth && !checkCollision(px + sDx, py)) {
            px = px + sDx;
          }
          if (Math.abs(dy) > 0.025 && py + sDy > 0 && py + sDy < mapHeight && !checkCollision(px, py + sDy)) {
            py = py + sDy;
          }
        }
        // }
        // if (cc.y > midX) {
        //   if (py + movementSpeed < mapHeight && !checkCollision(px, py + movementSpeed)) {
        //     py = py + movementSpeed;
        //   }
        // }
        // const diffX = (cc.x - midX) * movementSpeed;
        // const diffY = (cc.y - midY) * movementSpeed;
        // console.log('diff', diffX, diffY);
        // px = px + diffX;
        // py = py + diffY;
      }
      // if (keysPressed[' ']) { // SPACEBAR ATTACK
      //   console.log('TARGET', ct, ctt);
      //   if (playerCooldown <= 0) {
      //     playerCooldown = playerStartingCooldown;
      //     let closestMinionCount = 0;
      //     let closestMinionDistance = Infinity;
      //     am.map((m: Minion, count: number) => {
      //       const d = getDistance({x: px, y: py}, m.position)
      //       if (d < closestMinionDistance && m.health > 0) {
      //         closestMinionCount = count;
      //         closestMinionDistance = d;
      //       }
      //     });
      //     let closestTowerCount = 0;
      //     let closestTowerDistance = Infinity;''
      //     at.map((t: Tower, count: number) => {
      //       const d = getDistance({x: px, y: py}, t.position)
      //       if (d < closestTowerDistance && t.health > 0) {
      //         closestTowerCount = count;
      //         closestTowerDistance = d;
      //       }
      //     });
      //     // console.log('asd', closestMinionCount, closestMinionDistance)
      //     if (closestTowerDistance < playerRange) {
      //       at[closestTowerCount].getAttacked(20)
      //     } else {
      //       if (closestMinionDistance < playerRange) {
      //         // TODO: Player gets better range
      //         am[closestMinionCount].getAttacked(2); // TODO: Player gets stronger?
      //       }
      //     }
      //   }
      // }
      setPCooldown(playerCooldown);
    }
    const collisions = [
      // checkCollision(px, py),
      // checkCollision(px + PLAYER_SIZE , py),
      // checkCollision(px, py + PLAYER_SIZE),
      // checkCollision(px + PLAYER_SIZE, py + PLAYER_SIZE),
      // Blended families like this are very obvious (kids too close in age) and sometimes looked down on as kinda like slutty? But that usually happens when the first relationship fizzles early, the first kid (9 yr old Jaccard) is ready/well-cared for and the new love/relationship is super nurturing, childless and ready for love (Seah). Sometimes though, it does happen when the 1st relationship fizzles early, and the adult abandons the 1st kid to have a new family with the 2nd relationship, and then regrets it once the second kid is born. What Wiliw’s doing is just a super-asshole version
    ]
    if (checkCollision(px, py)) {
      // console.log('ehhh')
      // if (checkCollision(playerX, py)) {
      //   console.log('bad Y')
      //   py = py - movementSpeed; // reset y
      // }
      // if (checkCollision(px, playerY)) {
      //   console.log('bad X')
      //   px = px - movementSpeed; // reset x
      // }
    } else {
      if (checkType(px,py) === 2) {
        // console.log('healing', ph, playerHealth)
        ph = ph + 1
        if (ph > playerStartingHealth) {
          ph = playerStartingHealth;
        }
        setPlayerHealth(ph)
      }
      if (checkType(px,py) === 4) {
        // console.log('healing', ph, playerHealth)
        ph = ph - 1
        if (ph < 0) {
          ph = 0
          setPlayerHealth(0);
          ir = true;
          if (!isRespawning || !ir) {
            setTimeout(() => {
              console.log('respawn!!!')
              setPlayerX(0.5);
              setPlayerY(0.5);
              px = 0.5;
              py = 0.5;
              setPlayerHealth(playerStartingHealth);
  
              setClickCoords(false);
              cc = false;
              ph = playerStartingHealth;
              ir = false;
            }, 5000) // respawn takes 5s
          } else {
            // console.log('already dead')
          }
        }
        setPlayerHealth(ph)
      }
      setPlayerX(px);
      setPlayerY(py)
    }
    // console.log('tick.')
    // Update other game state based on delta and player input
    // ...
    const newGrid = grid.clone()
    // newGrid.setWalkableAt(Math.floor(px), Math.floor(py))
    updateMinionState(delta, lastTimestamp, newGrid, {x: px, y: py});
  }

  //
  const respawnRate = 10; //s
  // Enemy Minions
  const bruh = new MinionFactory(4, startA, respawnRate * 1000, basicMinionTypes);
  const handleNewMinion = (minion: Minion) => {
    am = [...am, minion];
    setActiveMinions((prevMinions) => {
      return ([...prevMinions, minion])
    }); 
  };
  bruh.onNewMinion(handleNewMinion)
  // Player Minions
  const breh = new MinionFactory(2, startB, respawnRate * 1000, basicMinionTypes); // terribleMinionTypes
  const handleMyNewMinion = (myMinion: Minion) => {
    mm = [...mm, myMinion];
    setMyMinions((prevMyMinions) => {
      return ([...prevMyMinions, myMinion])
    }); 
  }
  breh.onNewMinion(handleMyNewMinion);

  const broh = new TowerFactory(5);
  const handleNewTower = (tower: Tower) => {
    let dupTower = false;
    at.map((t: Tower) => {
      if (t.position.x === tower.position.x && t.position.y === tower.position.y) {
        dupTower = true;
      }
    });
    if (!dupTower) {
      at = [...at, tower];
      setActiveTowers((prevTowers) => {
        return ([...prevTowers, tower])
      }); 
    } else {
      console.log('Doop tower!!');
    }
  };
  broh.onNewTower(handleNewTower);
  const brah = new TowerFactory(3);
  const handleMyNewTower = (tower: Tower) => {
    let dupTower = false;
    mt.map((t: Tower) => {
      if (t.position.x === tower.position.x && t.position.y === tower.position.y) {
        dupTower = true;
      }
    });
    if (!dupTower) {
      mt = [...mt, tower];
      setMyTowers((prevTowers) => {
        return ([...prevTowers, tower])
      }); 
    } else {
      console.log('Doop tower!!');
    }
  };
  brah.onNewTower(handleMyNewTower);
  
  
  // 
  function restartTheGame() {
    handleInput();
    stopGameLoop();
    setActiveMinions([]);
    setMyMinions([]);
    setActiveTowers([]);
    setMyTowers([]);
    bruh.clearMinions();
    breh.clearMinions();
    broh.clearTowers();
    brah.clearTowers();
    console.log('uh huh', lastTimestamp, gameTime);
    setStartTime(lastTimestamp);
    lastTimestamp = 0;
    setGameTime(0);
    setGameOver(false);
    setGameState('Play')
    // startGameLoop();
    bruh.spawnOne();
    bruh.startSpawning(); //
    bruh.spawnForestMinions()
    breh.spawnOne();
    breh.startSpawning(); //
    broh.spawnTowers();
    brah.spawnTowers();
    startGameLoop();
  }

  useEffect(() => {
    console.log('uhhh');
    // restartTheGame();
    
    
    // bruh.spawnOne();
    // bruh.startSpawning();
    // bruh.startSpawning();
    // bruh.startSpawning();

    
    // breh.spawnOne();
    // breh.startSpawning(); // TODO: Spawns double???
    // breh.startSpawning(); // TODO: Spawns double???

    
    // bruh.startSpawning()
    return () => {
      // stopGameLoop();
      // bruh.offNewMinion(handleNewMinion)
      unhandleInput();
      // window.removeEventListener('keydown', handleKeyDown);
      // window.removeEventListener('keyup', handleKeyUp);
    }
  }, [])


  const renderMap = (playerX: number, playerY: number, minionPaths: number[][], clickCoords: ClickCoords) => {
    // let minionPaths = new Array(mapHeight).fill(new Array(mapWidth).fill(0));
    
    // console.log('yo', minionPaths);
    // minionPaths.map(x => console.log(x))
    // console.log('yoyo', paths);
    const coordsToClick = clickCoords && {
      x: clickCoords.x < 0 ? 0 : clickCoords.x >= mapWidth ? mapWidth-1 : clickCoords.x,
      y: clickCoords.y < 0 ? 0 : clickCoords.y >= mapHeight ? mapHeight-1 : clickCoords.y,
    }
    return <div className={styles.mapContainer} style={{
      transform: `translateX(${((playerX) * -TILE_SIZE)}px) translateY(${(playerY * -TILE_SIZE)}px)`,
    }}>
      {mapData.map((row: [], y: number) => (
        <div key={y} className={styles.mapRow}>
          {row.map(({type: tileType, collision}, x) => {
            let bgColor = 'rebeccapurple';
            if (tileType === 1) {
              bgColor = 'navy';
            } else if (tileType === 2) {
              bgColor = 'green';
            } else if (tileType === 3) {
              bgColor = 'brown';
            } else if (tileType === 4) {
              bgColor = 'darkgreen';
            } else if (tileType === 5) {
              bgColor = 'darkbrown';
            } else if (tileType === 6) {
              bgColor = 'purple';
            } else if (Math.floor(playerX) === x && Math.floor(playerY) === y) {
              // bgColor = 'purple';
            } else if (clickCoords) {
              if (coordsToClick && Math.floor(coordsToClick.x) === x && Math.floor(coordsToClick.y) === y) {
                // bgColor = 'violet';
              }
            } else {
              // console.log('minionPaths');
              // minionPaths.map(x => console.log(x))
              // if (minionPaths[y][x] === 1) {
              //   // console.log('yo', x, y);
              //   // bgColor = 'crimson';
              //   bgColor = 'firebrick';
              //   bgColor = 'purple';
              // }
            }
            return (
              <div
                key={x}
                // className={`map-tile tile-${tileType}`}
                className={tileType ? styles.mapTile : styles.mapTile2}
                style={{
                  backgroundColor: bgColor,
                  outline: collision ? '1px solid red' : undefined,
                  // transform: `translate(${(x * TILE_SIZE)}px, ${y * TILE_SIZE}px)`,
                }}
              />
            )
          })}
        </div>
      ))}
    </div>
  };


  const viewportStyle: React.CSSProperties = {
    display: 'flex',
    position: 'relative',
    width: '100vw',
    height: '100vh',
    overflow: 'hidden',
    // border: '2em inset gray',
    // perspective: '100px',
  };
  const miniMapStyle: React.CSSProperties = {
    background: 'rgba(254,213,70,0.1) -moz-Element(#article) no-repeat center / contain',
    position: 'fixed',
    right: '10px',
    top: '10px',
    outline: '1px dashed yellow',
  };
  const mapStyle: React.CSSProperties = {
    position: 'absolute',
    // top: (mapHeight * TILE_SIZE),
    // left: (mapWidth * TILE_SIZE),
    // left: 0,
    // top: 0,
    left: '50%',
    top: '50%',
    width: mapData[0].length * TILE_SIZE,
    height: mapData.length * TILE_SIZE,
    // transform: `translate(50%, 50%)`,
  };

  const restartButtonStyle: React.CSSProperties = {
    // fontSize: '10em',
    padding: '1em',
    transform: 'scale(2.5)',
    margin: '100%',
    // color: 'red',
  };
  const gameStateStyle: React.CSSProperties = {
    // fontSize: '10em',
    transform: 'scale(5)',
    paddingTop: '0.5em',
    // color: 'red',
  };
  const gameOverStyle: React.CSSProperties = {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    top: '25%',
    left: '50%',
    zIndex: 9000,
    textAlign: 'center',
  };
  const gameTimeStyle: React.CSSProperties = {
    position: 'absolute',
    top: '0%',
    left: '50%',
    fontSize: '1.5em',
    zIndex: 9000,
    textAlign: 'center',
  }
  const fpsStyle: React.CSSProperties = {
    position: 'absolute',
    top: '95%',
    left: '0%',
    fontSize: '1.5em',
    zIndex: 9000,
  }
  const lhcStyle: React.CSSProperties = {
    position: 'absolute',
    top: '0%',
    left: '0%',
    fontSize: '1.5em',
    zIndex: 9000,
  }

  const {currentFps} = useFps(20);

  // const minionPaths = new Array(mapHeight).fill(new Array(mapWidth).fill(0));
  const minionPaths = Array.from(Array(mapHeight), () => Array(mapWidth).fill(0));
  // activeMinions.map(m => {
  //   if (!m.path) return;
  //   m.path.map(p => {
  //     console.log('P -> ', p)
  //     minionPaths[p[1]][p[0]] = 1;
  //     // paths += 1;
  //   })
  // });

  // console.log('yo_');
  // minionPaths.map(x => (console.log(x)));
  for (let i = 0; i < activeMinions.length; i++) {
    // console.log(`Minion #${i} - ${activeMinions[i].position.x}, ${activeMinions[i].position.y}`);
    const element = activeMinions[i];
    if (!element.path) continue;
    // console.log(element.path);
    for (let j = 0; j < element.path.length; j++) {
      const step = element.path[j];
      // console.log(`Path step ${j} - ${step[0]}, ${step[1]}`)
      const yCoord = step[1];
      const xCoord = step[0];
      minionPaths[yCoord][xCoord] = 1;
    }
  }
  // console.log('yo');
  // minionPaths.map(x => (console.log(x)));
  // console.log('yoyo', curTarget, curTargetType);
  
  return (
    <>
      {/* <div>{Math.floor(playerX)}, {Math.floor(playerY)} - {Math.floor(minionX)}, {Math.floor(minionY)}</div> */}
      <div style={viewportStyle}>
        <div style={gameTimeStyle}>
          {Math.floor((gameOver ? gameOverTime : gameTime)/1000/60)}m {Math.floor((gameOver ? gameOverTime : gameTime)/1000%60)}s
          <div>
            {Math.floor(startTime/1000/60)}m {Math.floor(startTime/1000%60)}s
          </div>
        </div>
        <div style={gameOverStyle}>
          <>
          <div>Towers: {myTowers.filter(x => x.health > 0).length}/{myTowers.length} vs. {activeTowers.filter(x => x.health > 0).length}/{activeTowers.length}</div>
          <div>Minions: {myMinions.filter(x => x.health > 0).length}/{myMinions.length} vs. {activeMinions.filter(x => x.health > 0).length}/{activeMinions.length}</div>
          </>
          {gameOver &&
            <>
              <h2>Game Over</h2>
              <h1 style={{
                color: gameState === 'Victory' ? 'green' : 'red',
                ...gameStateStyle
              }}>
                {gameState}
              </h1>
              <button
                type='button'
                style={restartButtonStyle}
                onClick={() => {
                  restartTheGame();
                }}
              >
                Play
              </button>
            </>
          }
        </div>
        <div style={fpsStyle}>
          FPS - {currentFps}
        </div>
        <div style={lhcStyle}>
          Last Hits - {lastHitCount}
        </div>
        <div ref={miniMapRef} style={miniMapStyle}>
          
        </div>
        <div ref={mapRef} style={mapStyle}>
          {renderMap(playerX, playerY, minionPaths, clickCoords)}
        </div>
        <div>
        {myTowers.map((t, count) => {
            if (t.health <= 0) return;
            const curCooldown = t.remainingCooldown
            return (
              <div
                key={`tower${count}`}
                style={{
                  width: '32px',
                  height: '32px',
                  outline: '1px solid grey',
                  backgroundColor: t.type === 'basic' ? 'orange' : 'pink',
                  borderRadius: '20%',
                  position: 'absolute',
                  left: '50%',
                  top: '50%',
                  transform: `translateX(${((playerX - t.position.x) * -TILE_SIZE + PLAYER_SIZE/2)}px) translateY(${((playerY - t.position.y) * -TILE_SIZE + PLAYER_SIZE/2)}px)`,
                }}
              >
                {curCooldown > 0 ?
                  <div className={styles.attack} /> :
                  <div className={styles.noAttack} />
                }
                <div style={{
                  position: 'relative',
                  width: PLAYER_SIZE * 2,
                  height: '7px',
                  borderRadius: 100,
                  backgroundColor: 'red',
                  zIndex: 2000,
                  // TODO: kinda wonky
                  transform: `translateY(-${PLAYER_SIZE}px) `
                }}>
                  <div style={{
                    position: 'relative',
                    left: 0,
                    // backgroundColor: `hsl(0 ${t.lastHit ? 100 : 50}% 50%)`, // m.lastHit ? 'limegreen' : 'red',
                    backgroundColor: 'limegreen',
                    height: t.lastHit ? '10px' : '7px',
                    borderRadius: 100,
                    width: PLAYER_SIZE * 2 * (t.health/t.startingHealth) * (t.lastHit ? 3 : 1),
                    zIndex: 2001,
                    fontSize: '0.75em',
                    whiteSpace: 'nowrap',
                    overflow: 'visible',
                  }} >{t.health} / {t.startingHealth}</div>
                </div>
                <div style={{
                  position: 'relative',
                  top: `${-(t.range+(TILE_SIZE*2))/2}px`,
                  left: `${-(t.range+(TILE_SIZE*2))/2}px`,
                  height: `${(t.range+(TILE_SIZE*2))+10}px`,
                  width: `${(t.range+(TILE_SIZE*2))+10}px`,
                  borderRadius: '100%',
                  outline: `1px dotted rgba(255,0,0,0.5)`,
                }} />
              </div>
            );
          })}
          {activeTowers.map((t, count) => {
            if (t.health <= 0) return;
            const curCooldown = t.remainingCooldown
            return (
              <div
                key={`tower${count}`}
                style={{
                  width: '32px',
                  height: '32px',
                  outline: '1px solid grey',
                  backgroundColor: t.type === 'basic' ? 'cyan' : 'teal',
                  borderRadius: '20%',
                  position: 'absolute',
                  left: '50%',
                  top: '50%',
                  transform: `translateX(${((playerX - t.position.x) * -TILE_SIZE + PLAYER_SIZE/2)}px) translateY(${((playerY - t.position.y) * -TILE_SIZE + PLAYER_SIZE/2)}px)`,
                }}
              >
                {curCooldown > 0 ?
                  <div className={styles.attack} /> :
                  <div className={styles.noAttack} />
                }
                <div style={{
                  position: 'relative',
                  width: PLAYER_SIZE * 2,
                  height: '7px',
                  borderRadius: 100,
                  backgroundColor: 'darkred',
                  zIndex: 2000,
                  // TODO: kinda wonky
                  transform: `translateY(-${PLAYER_SIZE}px) `
                }}>
                  <div style={{
                    position: 'relative',
                    left: 0,
                    backgroundColor: `hsl(0 ${t.lastHit ? 100 : 50}% 50%)`, // m.lastHit ? 'limegreen' : 'red',
                    height: t.lastHit ? '10px' : '7px',
                    borderRadius: 100,
                    width: PLAYER_SIZE * 2 * (t.health/t.startingHealth) * (t.lastHit ? 3 : 1),
                    zIndex: 2001,
                    fontSize: '0.75em',
                    whiteSpace: 'nowrap',
                    overflow: 'visible',
                  }} >{t.health} / {t.startingHealth}</div>
                </div>
                <div style={{
                  position: 'relative',
                  top: '-32px',
                  left: '-32px',
                  width: '64px',
                  height: '64px',
                  marginBottom: '-64px',
                  borderRadius: 100,
                  outline: curTarget === count && curTargetType === 'tower' ? '2px dashed limegreen' : '1px solid rgba(0,0,0,0)',
                }} />
                <div style={{
                  position: 'relative',
                  top: `${-(t.range+(TILE_SIZE*2))/2}px`,
                  left: `${-(t.range+(TILE_SIZE*2))/2}px`,
                  height: `${(t.range+(TILE_SIZE*2))+10}px`,
                  width: `${(t.range+(TILE_SIZE*2))+10}px`,
                  borderRadius: '100%',
                  outline: '1px dotted rgba(255,0,0,0.5)',
                }} />
              </div>
            );
          })}
          {activeMinions.map((m, count) => (
            <MinionSprite
              key={`enemyMinionSprite${count}`}
              enemyMinion={true}
              count={count}
              curTarget={curTarget}
              curTargetType={curTargetType}
              playerX={playerX}
              playerY={playerY}
              {...m}
            />
          ))}
          {myMinions.map((m, count) => (
            <MinionSprite
              key={`MyMinionSprite${count}`}
              enemyMinion={false}
              count={count}
              curTarget={curTarget}
              curTargetType={curTargetType}
              playerX={playerX}
              playerY={playerY}
              {...m}
            />
          ))}
          {/* {activeMinions.map((m, count) => {
            // console.log(m, count);a
            if (m.health <= 0) return;
            return (
              <div
                key={`minion${count}`}
                style={{
                  width: '32px',
                  height: '32px',
                  outline: curTarget === count && curTargetType === 'minion' ? '1px solid red' : '1px solid grey',
                  backgroundColor: m.type === 'basic' ? 'orange' : 'red',
                  borderRadius: '100%',
                  position: 'absolute',
                  left: '50%',
                  top: '50%',
                  transform: `translateX(${((playerX - m.position.x) * -TILE_SIZE)}px) translateY(${((playerY - m.position.y) * -TILE_SIZE)}px) rotate(${-m.direction}deg)`,

                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  overflow: 'visible',
                }}
              >
                  <div style={{ // minion eyeball
                    position: 'relative',
                    top: '0px',
                    left: '0px',
                    width: '10px',
                    height: '10px',
                    // height: `${PLAYER_SIZE/2}px`,
                    backgroundColor: m.type === 'lvl3' ? 'blue' : 'grey',
                    borderRadius: 100,
                  }} />
                  {m.remainingCooldown > 0 ?
                    <div className={styles.attack} /> :
                    <div className={styles.noAttack} />
                  }
                  <div style={{
                    position: 'relative',
                    width: PLAYER_SIZE * 2,
                    height: '7px',
                    borderRadius: 100,
                    backgroundColor: 'darkred',
                    zIndex: 2000,
                    // TODO: kinda wonky
                    transform: `rotate(${m.direction}deg) translateY(-${PLAYER_SIZE}px) `
                  }}>
                    <div style={{
                      position: 'relative',
                      left: 0,
                      backgroundColor: `hsl(0 ${m.lastHit ? 100 : 50}% 50%)`, // m.lastHit ? 'limegreen' : 'red',
                      height: m.lastHit ? '10px' : '7px',
                      borderRadius: 100,
                      width: PLAYER_SIZE * 2 * (m.health/m.startingHealth) * (m.lastHit ? 3 : 1),
                      zIndex: 2001,
                      fontSize: '0.75em',
                      whiteSpace: 'nowrap',
                      overflow: 'visible',
                    }} >{m.health} / {m.startingHealth}</div>
                  </div>
                  <div style={{
                    position: 'relative',
                    // top: '-32px',
                    // left: '-32px',
                    width: '64px',
                    height: '64px',
                    marginBottom: '-64px',
                    borderRadius: 100,
                    outline: curTarget === count && curTargetType === 'minion' ? '2px dashed limegreen' : '1px solid rgba(0,0,0,0)',
                    margin: 'auto',
                  }} /> 
                  <div style={{
                    position: 'relative',
                    // display: 'inline',
                    // top: `${-(m.range)/2}px`,
                    // left: `${-(m.range)/2}px`,
                    top: 0,
                    height: `${(m.range)}px`,
                    width: `${(m.range)}px`,
                    borderRadius: '100%',
                    outline: '1px dotted rgba(255,0,0,0.5)',
                    margin: 'auto',
                  }} />
                  <div style={{
                    position: 'relative',
                    // display: 'inline',
                    // top: `${-(m.vision)/2}px`,
                    // left: `${-(m.vision)/2}px`,
                    height: `${(m.vision)}px`,
                    width: `${(m.vision)}px`,
                    borderRadius: '100%',
                    outline: '1px dotted rgba(255,255,0,0.5)',
                    margin: 'auto',
                  }} />
                </div>
            );
          })} */}
          {/* {myMinions.map((m, count) => {
            // console.log(m, count);a
            if (m.health <= 0) return;
            return (
              <div
                key={`myMinion${count}`}
                style={{
                  width: '32px',
                  height: '32px',
                  outline: '1px solid grey',
                  backgroundColor: 'blue',
                  borderRadius: '100%',
                  position: 'absolute',
                  left: '50%',
                  top: '50%',
                  transform: `translateX(${((playerX - m.position.x) * -TILE_SIZE)}px) translateY(${((playerY - m.position.y) * -TILE_SIZE)}px) rotate(${-m.direction}deg)`,
                }}
              >
                <div style={{
                  position: 'relative',
                  top: '65%',
                  left: '50%',
                  width: '10px',
                  height: '10px',
                  // height: `${PLAYER_SIZE/2}px`,
                  backgroundColor: m.type === 'lvl3' ? 'blue' : 'grey',
                  borderRadius: 100,
                }} />
                {m.remainingCooldown > 0 ?
                  <div className={styles.attack} /> :
                  <div className={styles.noAttack} />
                }
                <div style={{
                  position: 'relative',
                  width: PLAYER_SIZE * 2,
                  height: '7px',
                  borderRadius: 100,
                  backgroundColor: 'darkred',
                  zIndex: 2000,
                  // TODO: kinda wonky
                  transform: `rotate(${m.direction}deg) translateY(-${PLAYER_SIZE}px) `
                }}>
                  <div style={{
                    position: 'relative',
                    left: 0,
                    backgroundColor: 'limegreen', // `hsl(0 ${m.lastHit ? 100 : 50}% 50%)`, // m.lastHit ? 'limegreen' : 'red',
                    height: m.lastHit ? '10px' : '7px',
                    borderRadius: 100,
                    width: PLAYER_SIZE * 2 * (m.health/m.startingHealth) * (m.lastHit ? 3 : 1),
                    zIndex: 2001,
                    fontSize: '0.75em',
                    whiteSpace: 'nowrap',
                    overflow: 'visible',
                  }} >{m.health} / {m.startingHealth}</div>
                </div>
                <div style={{
                  position: 'relative',
                  top: `${-(m.range)/2}px`,
                  left: `${-(m.range)/2}px`,
                  height: `${(m.range)}px`,
                  width: `${(m.range)}px`,
                  borderRadius: '100%',
                  outline: '1px dotted rgba(255,0,0,0.5)',
                }} />
              </div>
            );
          })} */}
        </div>
        {/* 
         * _PLAYER_
         */}
        <div
          ref={playerRef}
          className={styles.player}
          style={{
            backgroundColor: playerHealth <= 0 ? 'gray' : 'cyan',
            position: 'absolute',
            left: '50%', // TODO: Can we bias the camera to look more "down lane"
            top: '50%',
            transform: `translate(-50%, -50%)`, // TODO: Not perfectly centered...
          }}
        >
          <div style={{
            position: 'relative',
            top: `-${PLAYER_SIZE/2}px`,
            left: `-${PLAYER_SIZE/2}px`,
            width: PLAYER_SIZE * 2,
            height: '7px',
            borderRadius: 100,
            backgroundColor: 'red',
            zIndex: 2000,
          }}>
            <div style={{
              position: 'relative',
              left: 0,
              backgroundColor: 'limegreen',
              height: '7px',
              borderRadius: 100,
              width: PLAYER_SIZE * 2 * (playerHealth/playerStartingHealth),
              zIndex: 2001,
            }} >{playerHealth}</div>
            {pCooldown > 0 ?
              <div className={styles.attack} /> :
              <div className={styles.noAttack} />
            }
          </div>
          <div style={{
            position: 'relative',
            top: `${-(playerRange+TILE_SIZE)/2}px`,
            left: `${-(playerRange+TILE_SIZE)/2}px`,
            height: `${(playerRange+TILE_SIZE)+10}px`,
            width: `${(playerRange+TILE_SIZE)+10}px`,
            borderRadius: '100%',
            outline: '1px dotted rgba(255,0,0,0.5)',
          }} />
        </div>
        {/* <div
          ref={minionRef}
          // className={styles.minion}
          style={{
            width: '32px',
            height: '32px',
            backgroundColor: 'red',
            borderRadius: '100%',
            position: 'absolute',
            left: '50%',
            top: '50%',
            // dstransform: `translate(-50%, -50%)`,
            transform: `translateX(${((playerX - minionX) * -TILE_SIZE)}px) translateY(${((playerY - minionY) * -TILE_SIZE)}px)`,
          }}
        /> */}
      </div>
    </>
  );
}

interface MinionSpriteProps {
  // TODO IS a tower just a minion that cant move?
  enemyMinion: boolean,
  count: number,
  curTarget: any,
  curTargetType: string,
  playerX: number,
  playerY: number,
  // ...MinionProps,
}

const MinionSprite = (props: any) => {
  const {
    enemyMinion,
    count,
    curTarget,
    curTargetType,
    playerX,
    playerY,
    ...rest
  } = props;
  if (rest.health <= 0) return;
  return (
    <div // Minion
      key={`${enemyMinion ? 'enemy' : 'my'}Minion${count}`}
      style={{
        width: '32px',
        height: '32px',
        // outline: curTarget === count && curTargetType === 'minion' ? '1px solid red' : '1px solid grey',
        backgroundColor: enemyMinion ? 'orange' : 'cyan',
        borderRadius: '100%',
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: `translateX(${((playerX - rest.position.x) * -TILE_SIZE)}px) translateY(${((playerY - rest.position.y) * -TILE_SIZE)}px) rotate(${-rest.direction}deg)`,
        
        // overflow: 'visible',
      }}
    >
      <div style={{
          position: 'absolute',
          top: '-16px',
          left: '-16px',
          width: '64px',
          height: '64px',
          // marginBottom: '-64px',
          borderRadius: 100,
          outline: enemyMinion && curTarget === count && curTargetType === 'minion' ? '2px dashed limegreen' : '1px solid rgba(0,0,0,0)',
          // margin: 'auto',
        }} >
      <div style={{
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <div style={{ // Ninion eyeball
          position: 'absolute',
          top: '40px', // TODO: ehhh good enough
          left: '20px',
          width: '10px',
          height: '10px',
          backgroundColor: 'black',
          borderRadius: 100,
        }} />
        
        <div style={{ // Minion Health
          margin: '1em',
          position: 'absolute',
          top: '50%',
          left: '-50%',
          width: PLAYER_SIZE * 2,
          height: '7px',
          borderRadius: 100,
          backgroundColor: 'darkred',
          zIndex: 2000,
          // TODO: kinda wonky
          transform: `rotate(${props.direction}deg) translateY(-${PLAYER_SIZE}px) `,
          // outline: '1px solid limegreen',
        }}>
          <div style={{
            position: 'relative',
            left: 0,
            backgroundColor: `hsl(0 ${props.lastHit ? 100 : 50}% 50%)`, // props.lastHit ? 'limegreen' : 'red',
            height: props.lastHit ? '10px' : '7px',
            borderRadius: 100,
            width: PLAYER_SIZE * 2 * (props.health/props.startingHealth) * (props.lastHit ? 3 : 1),
            zIndex: 2001,
            fontSize: '0.75em',
            whiteSpace: 'nowrap',
            overflow: 'visible',
          }} >{props.health} / {props.startingHealth}</div>
        </div>

        {rest.remainingCooldown > 0 ? // Attack "animation"
          <div className={styles.attack} /> :
          <div className={styles.noAttack} />
        }
        <div style={{
          position: 'absolute',
          // display: 'inline',
          top: `${-(rest.range)/6}px`, // TODO: why divided by 6 ???
          left: `${-(rest.range)/6}px`,
          // top: 0,
          height: `${(rest.range)}px`,
          width: `${(rest.range)}px`,
          borderRadius: '100%',
          outline: '1px dotted rgba(255,0,0,0.5)',
          // margin: 'auto',
          background: 'radial-gradient(rgba(255,0,0,0.25), rgba(255,0,0,0.05))',
        }}>
          <div style={{
            position: 'absolute',
            // display: 'inline',
            top: `${-(rest.vision)/3}px`, // TODO: why 3 what is going onnnn?
            left: `${-(rest.vision)/3}px`,
            // top: '50%',
            height: `${(rest.vision)}px`,
            width: `${(rest.vision)}px`,
            borderRadius: '100%',
            outline: '1px dotted rgba(255,255,0,0.5)',
            margin: 'auto',
          }} />
         </div>
      </div>
      </div>
    </div>
  );
}